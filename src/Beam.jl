# Author:    Matthieu Melennec
# Created:   22 September 2022
# Updated:   29 September 2022

"""
$(TYPEDEF)

Bundles up useful date on a Beam.

# Fields
$(FIELDS)
"""
mutable struct Beam
    """Particle type."""
    ptype::Particle
    """Kinetic energy fo the Beam."""
    ekin::Real
end

"""
$(TYPEDEF)

Bundles up useful data on a Bunch.

# Fields
$(FIELDS)
"""
mutable struct Bunch
    """Phase-space coordinates of the particles in the Bunch."""
    particle_ps::Matrix{<:Real}
    """Beam data."""
    beam::Beam
end