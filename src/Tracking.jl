# Author:    Matthieu Melennec
# Created:   29 September 2022
# Updated:   26 October 2022

"""
$(SIGNATURES)

A function to evolve the Bunch pbunch through Element E.

# Examples
```jldoctest

julia> E = Drift(1.0)

julia> beam = Beam(Proton(), 100.0)

julia> particle_ps = genDistrib(Gaussian([0,0,0,0,0,0], Diagonal([1,1,1,1,1,1])), 10)
6×10 Matrix{Float64}:
  1.83278    0.10699    0.115376    0.182838   0.439386    1.17466    2.11379   -0.793685    1.37662   -0.75615
  0.176864  -0.771733   0.267182    0.647011  -0.999171    2.6622     0.214626   1.91667    -1.02787   -0.71563
  0.212755  -0.935278  -0.717065    0.6209     1.49714    -0.391487   0.805219   0.271588    0.770692  -0.653488
  0.919473   0.057881  -1.34053     0.02274    0.0917504  -0.594363  -0.245191  -1.03106     0.466904  -1.77017
  0.914143   0.337736   0.0476516  -0.235162  -1.17648     1.82336    0.860836  -0.373162   -0.116301  -0.291607
 -0.930385  -0.543424  -0.641232   -0.18575    0.9966     -0.119407  -0.225226   0.0651508   1.13111    1.21149

julia> pbunch = Bunch(particle_ps, beam)

julia> evolve(E, pbunch)
6×10 Matrix{Float64}:
  2.00965    -0.664744   0.382558         0.829849  -0.559785    3.83686     2.32842     1.12298     0.348745   -1.47178
  0.176864   -0.771733   0.267182         0.647011  -0.999171    2.6622      0.214626    1.91667    -1.02787    -0.71563
  1.13223    -0.877397  -2.0576           0.64364    1.58889    -0.98585     0.560028   -0.759471    1.2376     -2.42366
  0.919473    0.057881  -1.34053          0.02274    0.0917504  -0.594363   -0.245191   -1.03106     0.466904   -1.77017
 -4.36477e6  -2.5494e6  -3.00825e6  -871421.0        4.67541e6  -5.60178e5  -1.05662e6   3.05646e5   5.30645e6   5.68353e6
 -0.930385   -0.543424  -0.641232        -0.18575    0.9966     -0.119407   -0.225226    0.0651508   1.13111     1.21149
```
"""
function evolve(M::Matrix, pbunch::Bunch)
    Γ = pbunch.particle_ps
    N = size(Γ)[2]
    for i in 1:N
         ξ = Γ[:,i]
         ξ_f = M * ξ
         Γ[:,i] = ξ_f
    end
    pbunch.particle_ps = Γ
    return Γ
end