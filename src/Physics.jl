# Author:    Matthieu Melennec
# Created:   22 September 2022
# Updated:   26 October 2022

"""
$(SIGNATURES)

Function to calculate the Lorentz Factor.
"""
function get_gamma(ekin, mass)
    return (ekin / mass) + 1
end

get_gamma(B::Beam) = get_gamma(B.ekin, B.ptype.mass)

"""
$(SIGNATURES)

Calculate ``β`` from the Lorentz factor.
"""
function get_beta(γ)
    return sqrt(1- 1/γ^2)
end