# Author:    Matthieu Melennec
# Created:   21 September 2022
# Updated:   22 September 2022

abstract type Particle end

"""
$(TYPEDEF)

A Porton instance of a particle. It holds the data related to protons.

# Examples
```jldoctest

julia> prot = Proton()
Proton(9.382720881604904e8, 1.602176634e-19, "Proton")
```
"""
struct Proton <: Particle
    mass
    charge
    pname
    Proton() = new(ustrip(natural(+ProtonMass)), ustrip(+ElementaryCharge), "Proton")
end

"""
$(TYPEDEF)

An Electron instance of a particle. It holds the data related to electrons.

# Examples
```jldoctest

julia> prot = Electron()
Electron(510998.9499961642, -1.602176634e-19, "Electron")
"""
struct Electron <: Particle
    mass
    charge
    pname
    Electron() = new(ustrip(natural(+ElectronMass)), ustrip(-ElementaryCharge), "Electron")
end

function printP(P::Particle)
    print("        Particle:    ")
    println(P.pname)
    print("            * charge:    ")
    println(P.charge)
    print("            * mass:      ")
    println(P.mass)
end

Base.:print(P::Particle) = printP(P)
