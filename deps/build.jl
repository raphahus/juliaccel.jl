using Pkg

deps = [
    "Random",
    "Distributions",
    "PhysicalConstants",
    "NaturallyUnitful",
    "DocStringExtensions",
    "LaTeXStrings"
]

Pkg.add(deps)
